create DATABASE usermanagement;

create table user(
	ID SERIAL PRIMARY KEY AUTO_INCREMENT,
	login_id VARCHAR(255) UNIQUE NOT NULL,
	name VARCHAR(255) NOT NULL,
	birth_date DATE NOT NULL,
	password VARCHAR(255) NOT NULL,
	is_admin boolean NOT NULL,
	create_date DATETIME NOT NULL,
	update_date DATETIME NOT NULL);
